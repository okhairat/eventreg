var path = require('path');
const Sequelize = require('sequelize');
var dateFormat = require('dateformat');

const dbPath = path.resolve(__dirname, 'visitors.db')
let connection = new Sequelize('', '', '', {
    host: 'localhost',
    dialect: 'sqlite',
    operatorsAliases: false,
    // SQLite only
    storage: dbPath,
});

connection.authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  });

function isEID(val) {
    str = ''+val
    str = str.replace(/\s/g,'').replace(/-/g,'');
    if(str.startsWith('784') && str.length == 15) {
        return Number(str);
    }
    return false;
}

String.prototype.splice = function(idx, rem, str) {
    return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};

function isUAEmobile(val) {
    str = ''+val;
    str = str.replace(/\s/g,'').replace(/-/g,'');
    if(str.startsWith('009715') && str.length == 14) {
        return str.slice(2);
    } else if(str.startsWith('+9715') && str.length == 13) {
        return str.slice(1);
    } else if(str.startsWith('9715') && str.length == 12) {
        return str;
    } else if(str.startsWith('05') && str.length == 10) {
        return '971'+str.slice(1);
    } else if(str.startsWith('5') && str.length == 9) {
        return '971'+str;
    } else {
        return false;
    }
}

let Visitor = connection.define('visitor', {
    id: { 
        type: Sequelize.INTEGER, 
        autoIncrement: true ,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    name_ar: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    eid: {
        type: Sequelize.INTEGER(15).UNSIGNED,
        allowNull: false,
        validate: {
            isNumeric: { msg: "Must be an integer"},
        },
        get() {
            return this.getDataValue('eid').toString().splice(3,0,"-").splice(8,0,"-").splice(-1,0,"-");
        },
        set(val) {
            this.setDataValue('eid', isEID(val));
        }
    },
    phone: {
        type: Sequelize.INTEGER(12).UNSIGNED,
        allowNull: false,
        validate: {
            isInt: { msg: "Must be an integer"},
        },
        set(val) {
            this.setDataValue('phone', parseInt(isUAEmobile(val)));
        }
    },
    badge: {
        type: Sequelize.STRING,
        allowNull: true
    },
    hall: {
        type: Sequelize.STRING,
        allowNull: true
    },
    zone: {
        type: Sequelize.STRING,
        allowNull: true
    },
    seat: {
        type: Sequelize.STRING,
        allowNull: true
    },
    // date: {
    //     type: Sequelize.DATE,
    //     allowNull: true,
    //     validate: {
    //         isDate: true
    //     },
    //     get() {
    //         return this.getDataValue('date') ? dateFormat(new Date(this.getDataValue('date')), "yyyy/mm/dd"):'';
    //     }
    // },
    attended: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
        get() {
            return this.getDataValue('attended') ? 'YES':'NO';
        },
    }
  },{
    timestamps: false
});

// force: true will drop the table if it already exists
Visitor.sync({force: false}).then(() => {
    // Visitor.create({ name: 'John Doe', eid: '784199783027511', phone: 971508282129 })
  });


connection.sync({logging: console.log});
module.exports = Visitor;