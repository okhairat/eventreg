import numpy as np
import pandas as pd
from sqlalchemy import create_engine

engine = create_engine('sqlite:////Users/omar/Development/NAMSHI/tests/eventreg/models/visitors.db')
connection = engine.connect()
engine.execute("SELECT * FROM visitors limit 10").fetchall()

visitors_ain = pd.read_excel('/Users/omar/Downloads/visitors.xlsx', sheetname=1)
visitors_ain = visitors_ain[visitors_ain['Jobseeker No.'] != 'Empty Seat']
visitors_ain['Hall'] = 'Al Ain'
visitors_ain = visitors_ain.rename(index=str, columns={"Full Name(Ar)": "name_ar","NSR Number": "eid", "Mobile": "phone", "Hall":"hall","Zone / Column":"zone","Seat":"seat"})
eiman = visitors_ain[visitors_ain['phone'] == '00971554200041']
visitors_ain = visitors_ain[visitors_ain['seat'] != 251]

eiman['eid'] = '784000000000000'
#eiman = eiman.rename(index=str, columns={"Full Name(Ar)": "name_ar","NSR Number": "eid", "Mobile": "phone", "Hall":"hall","Zone / Column":"zone","Seat":"seat"})

cleaned = visitors_ain[["name_ar","eid","phone","hall","zone","seat"]]
cleaned['id'] = None
cleaned['name'] = None
cleaned['badge'] = 'ain'
cleaned['attended'] = 0



cleaned.to_sql('visitors', con=engine, if_exists='append', index=False)


visitors_abd = pd.read_excel('/Users/omar/Downloads/visitors.xlsx', sheetname=0)
visitors_abd = visitors_abd[visitors_abd['Jobseeker No.'] != 'Empty Seat']
visitors_abd = visitors_abd.rename(index=str, columns={"Full Name(Ar)": "name_ar","NSR Number": "eid", "Mobile": "phone", "Hall":"hall","Zone / Column":"zone","Seat":"seat"})

cleaned = visitors_abd[["name_ar","eid","phone","hall","zone","seat"]]
cleaned['id'] = None
cleaned['name'] = None
cleaned['badge'] = 'abd'
cleaned['attended'] = 0


visitors_all = pd.read_excel('/Users/omar/Downloads/visitors.xlsx', sheetname=0)
visitors_all = visitors_all[visitors_all['Jobseeker No.'] != 'Empty Seat']
visitors_all = visitors_all.rename(index=str, columns={"Full Name(Ar)": "name_ar","NSR Number": "eid", "Mobile": "phone", "Hall":"hall","Zone / Column":"zone","Seat":"seat"})

cleaned = visitors_all[["name_ar","eid","phone","hall","zone","seat"]]
cleaned['id'] = None
cleaned['name'] = None
cleaned['badge'] = 'all'
cleaned['attended'] = 0



cleaned.to_sql('visitors', con=engine, if_exists='append', index=False)


