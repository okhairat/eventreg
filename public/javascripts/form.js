'use strict'

$(document).ready(function() {
    var $form=$('#search-form');
    var $keyword=$('input[name=keyword]');
    $form.submit(function(e) {
        $('#search-form .alerts').html('')
        if($keyword.val().length == 0)
        {
            console.log('show invalid');
            $('.invalid-feedback').show();
        }
        else
        {
            var $cardsWrapper = $('.results .cards-wrapper');
            var $cardsLoader = $('.results .cards-loader');
            $.post('/search', { 'keyword': $keyword.val() }, function(data){
                $cardsLoader.show();
                $cardsWrapper.html(data);
                $cardsLoader.hide();                
            }).fail(function(data){
                console.log('fail');
                console.log(data.responseText);
                $('#search-form .alerts').html(data.responseText)
            });
        }
        e.preventDefault();
    });

    $keyword.on('input', function () {
        $('.invalid-feedback').hide();
        if($keyword.val().length > 2)
        {
            if($keyword.val().startsWith('971') || $keyword.val().startsWith('0097') ||
            $keyword.val().startsWith('+97') || $keyword.val().startsWith('05') || $keyword.val().startsWith('5'))
            {
                $('#search-form .input-group .input-group-prepend i.fas').hide();
                $('#search-form .input-group .input-group-prepend i.fas.fa-mobile').show();
            } else if($keyword.val().startsWith('784')) {
                $('#search-form .input-group .input-group-prepend i.fas').hide();
                $('#search-form .input-group .input-group-prepend i.fas.fa-address-card').show();
            } else {
                $('#search-form .input-group .input-group-prepend i.fas').hide();
                $('#search-form .input-group .input-group-prepend i.fas.fa-ad').show();
            }
        }
    });
    $('#search-form .input-group i.fas.fa-search').show();
});