$(document).ready(function() {
    var table = $('#allcards').DataTable({
        data: $data,
        columns: [
            { data: 'Name (Ar)' },
            { data: 'Phone' },
            { data: 'EmiratesID' },
            { data: 'Hall' },
            { data: 'Zone'},
            { data: 'Seat'},
            { data: 'Attended' },
            { 
                data: null,
                defaultContent: '<button type="button" class="btn btn-outline-secondary table-view">view</button>'
            }
        ],
        buttons: ['excel', 'pdf', 'print']
    });
 
    table.buttons().container()
        .appendTo( '#allcards_wrapper:eq(0)' );

    $('#allcards tbody').on( 'click', 'button', function () {
        var data = table.row( $(this).parents('tr') ).data();
        // console.log(data['Action']);
        window.location.replace('/card/'+data['Action']);
    } );
} );