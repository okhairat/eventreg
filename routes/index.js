var express = require('express');
var router = express.Router();
let Visitor = require('../models/visitor');
const validator = require('validator');
const Sequelize = require('sequelize');

function isUAEmobile(str) {
  str = str.replace(/\s/g,'');
  if(str.startsWith('009715') && str.length == 14) {
    return str.slice(2);
  } else if(str.startsWith('+9715') && str.length == 13) {
    return str.slice(1);
  } else if(str.startsWith('9715') && str.length == 12) {
    return str;
  } else if(str.startsWith('05') && str.length == 10) {
    return '971'+str.slice(1);
  } else if(str.startsWith('5') && str.length == 9) {
    return '971'+str;
  } else {
    return false;
  }
}

function getAllMobileformats(str) {
  if(str.startsWith('9715') && str.length == 12) {
    return [str, '0'+str.slice(3), str.slice(3)];
  }
}

function isEID(str) {
  str = str.replace(/\s/g,'').replace(/-/g,'');
  if(str.startsWith('784') && str.length == 15) {
    return str;
  }
  return false;
}

/* GET home page. */
router.get('/', function(req, res, next) {

  res.render('form', { title: 'Event'});
});

router.get('/all/cards', async function(req, res, next) {
  visitors = await Visitor.findAll();
  group=[]
  visitors.forEach(visitor => {
    group.push({
      'Name (Ar)': visitor.name_ar,      
      'Phone': visitor.phone,
      'EmiratesID': visitor.eid,
      'Hall': visitor.hall,
      'Zone': visitor.zone,
      'Seat': visitor.seat,
      'Attended': visitor.attended,
      'Action': visitor.id
    });
  });
  res.render('allcards', { data: group});
});

router.get('/search', function(req, res, next) {
  res.render('form', { title: 'Event'});
});

router.post('/update/card', function(req, res, next) {
  id = req.body.id;
  Visitor.findById(id).then(visitor => {
    visitor.update({
      attended: 1
    }).then(() => {});
  });
});

router.post('/search', async function(req, res, next) {
  keyword = req.body.keyword;
  visitors= [];
  str = await keyword.replace(/\s/g,'').replace(/-/g,'');
  Op = Sequelize.Op;
  if(await validator.isAlpha(str)) {
    names = keyword.split(" ");
    whereStatment = Array();
    names.forEach(name => {
      whereStatment.push({name: {[Op.like]: '%'+name+'%'}})
    });

    visitors = await Visitor.findAll({
      where: whereStatment,
      limit: 5
    });
  } else if(await validator.isAlpha(str, ["ar-AE"])){
    names = keyword.split(" ");
    whereStatment = Array();
    names.forEach(name => {
      whereStatment.push({name_ar: {[Op.like]: '%'+name+'%'}})
    });

    visitors = await Visitor.findAll({
      where: whereStatment,
      limit: 5
    });
  } else if(await validator.isNumeric(str))
  {
    if(number = isUAEmobile(str))
    {
      phones = getAllMobileformats(number);
      whereStatment = Array();
      phones.forEach(phoneFormat => {
        whereStatment.push({phone: phoneFormat})
      });
      visitors = await Visitor.findAll({
        where: {
          [Op.or]: whereStatment
        }
      });
    } else if(eid = isEID(str)) {
      visitors = await Visitor.findAll({
        where: {
          eid: eid
        },
        limit: 5
      });
    } else {
      res.status(400);
      res.render('form-alert', { error: true, msgTitle: 'Not valid input', 
                msg: 'please input a valid name as registered on emirates ID or a UAE mobile \
                number or an emirates id number formated as 784-1980-XXXXXXX-X'}, function(err, alert){
                  res.send(alert);
                });
    }
  } else {
    res.status(400);
    res.render('form-alert', { error: true, msgTitle: 'Not valid input', 
              msg: 'please input a valid name as registered on emirates ID or a UAE mobile \
              number or an emirates id number formated as 784-1980-XXXXXXX-X'}, function(err, alert){
                res.send(alert);
              });
  }
  res.render('cardlist', {visitors: visitors}, function(err, cards){
    res.send(cards);
  });
});

router.get('/card/:id', async function(req, res, next) {
  try {
    var { id } = req.params;
    var visitor = await Visitor.findOne({ where: {id: id}});
    res.render('card', { visitor: visitor});
  } catch(err) {
    err.message= `card ${id} does not exist!`;
    err.status=404;
    next(err);
  }
});

module.exports = router;
